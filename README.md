Calcul des valeurs propres d'une matrice Non-Hermitienne par une méthode de
Krylov!!!!


Descriptif:
Les problèmes aux valeurs propres peuvent apparaître dans différents types d'application comme par
exemple l'analyse des vibrations, l'analyse de stabilité d'un réseau électrique, les problèmes issus de
l'équation de Schrödinger en mécanique quantique etc...
On s'intéresse dans ce projet à l'étude de méthodes de Krylov pour le calcul aux valeurs propres de
matrices Non-Hermitiennes : l'algorithme d'Arnoldi et de Lanczos pour les matrices non-Hermitiennes.